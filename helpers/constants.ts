export const READING_MODE = "reading";
export const LISTENING_MODE = "listening";

export const SOURCE_INSTAGRAM = "instagram";
export const SOURCE_FACEBOOK = "facebook";
export const SOURCE_GOOGLE = "google";
