import Image from "next/image";
import React from "react";

import { Typography } from "../Typography/Typography";
import { TestimonialCardProps } from "./TestimonialCard.props";
import { SOURCE_INSTAGRAM } from "../../helpers/constants";

import IconStarFilled from "../../assets/images/homePage/star-fill.svg";
import IconInstagram from "../../assets/images/homePage/instagram.svg";

import s from "./TestimonialCard.module.scss";

export function TestimonialCard({
  content,
  userImg,
  origin,
  rating,
}: TestimonialCardProps) {
  const constructRating = (): JSX.Element[] => {
    const stars: JSX.Element[] = [];
    for (let i = 0; i < rating; i++) {
      stars.push(
        <IconStarFilled />,
      );
    }
    return [...stars];
  };

  return (
    <div className={s.testimonialCard}>
      <div className={s.testimonialRating}>
        {/* <IconStarFilled />
        <IconStarFilled />
        <IconStarFilled />
        <IconStarFilled />
        <IconStarFilled /> */}
        {constructRating()}
      </div>
      <Typography tag="p" className={s.testimonialComment}>
        This app simplifies books into super condensed but easy-to-digest
        snippets. Listened to almost all of the Art of Saying No during my warm
        up/stretch sesh today. A powerful tool I recommend to anyone who’s busy
        and can’t find time to sit down to read!
        {content}
      </Typography>
      <div className={s.testimonialUser}>
        <Image width={32} height={32} src={userImg} alt="User Profile" />
        <Typography tag="span" className={s.testimonialUserName}>
          mr.rageright
        </Typography>
        {origin === SOURCE_INSTAGRAM && <IconInstagram />}
      </div>
    </div>
  );
}
