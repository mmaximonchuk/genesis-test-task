import { DetailedHTMLProps, HTMLAttributes } from "react";

export interface TestimonialCardProps
  extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
  content: string;
  userImg: StaticImageData;
  origin: string;
  rating: number;
}
