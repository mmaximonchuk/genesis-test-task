import { DetailedHTMLProps, HTMLAttributes } from "react";
import { CategoriesText } from "../CategotyTag/CategotyTag.props";

export interface BookSnippetProps
  extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
  activeBooks: Books[];
  activeCategories: Category[];
  setActiveBooks: React.Dispatch<
    React.SetStateAction<
      {
        id: number;
        bookImg: React.FunctionComponent<React.SVGAttributes<SVGAElement>>;
        categories: CategoriesText[];
        isActive: boolean;
      }[]
    >
  >;
}

export type Category = {
  categoryText: CategoriesText;
  CategoryImg: React.FunctionComponent<React.SVGAttributes<SVGAElement>> | null;
  isActive: boolean;
};

export type Books = {
  id: number;
  bookImg: React.FunctionComponent<React.SVGAttributes<SVGAElement>>;
  categories: CategoriesText[];
  isActive: boolean;
};
