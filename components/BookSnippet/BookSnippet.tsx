import React, { useEffect, useState } from "react";
import cn from "classnames";

import { BookSnippetProps, Category } from "./BookSnippet.props";

import s from "./BookSnippet.module.scss";
import { CategoriesText } from "../CategotyTag/CategotyTag.props";

function BookSnippets({
  activeBooks,
  activeCategories,
  setActiveBooks,
}: BookSnippetProps) {
  const [offsetPage, setOffsetoffsetPage] = useState<number>(0);
  const [pageHeight, setPageHeight] = useState<number>(0);
  const [isRestoredColor, setIsRestoredColor] = useState<boolean>(true);

  const handleActiveBooks = () => {
    const choosenCategories: CategoriesText[] = activeCategories
      .filter((c) => c.isActive === true)
      .map((c: Category) => c.categoryText);

    if (choosenCategories.length > 0) {
      setIsRestoredColor(false);
    } else {
      setIsRestoredColor(true);
    }

    const updatedBooks = activeBooks.map((book) => {
      for (let i = 0; i < choosenCategories.length; i++) {
        if (book.categories.some((c) => c === choosenCategories[i])) {
          return {
            ...book, isActive: true,
          };
        }
      }
      return {
        ...book, isActive: false,
      };
    });

    setActiveBooks(updatedBooks);
  };

  const handleScroll = () => {
    setOffsetoffsetPage(window.pageYOffset);
  };

  useEffect(() => {
    setPageHeight(document.body.clientHeight);
    window.addEventListener("scroll", handleScroll);
    return () => window.removeEventListener("scroll", handleScroll);
  }, []);

  useEffect(() => {
    handleActiveBooks();
  }, [activeCategories]);

  return (
    <div className={s.bookSnippets}>
      <div
        className={s.top}
        style={{
          left: -((offsetPage - 300) / pageHeight) * 100,
        }}
      >
        {activeBooks.slice(0, 4).map((book) => (
          <div
            key={book.id}
            className={cn(s.snippet, {
              [s.fadedBook]: !book.isActive,
              [s.restoredColor]: isRestoredColor,
            })}
          >
            <book.bookImg />
          </div>
        ))}
      </div>
      <div
        className={s.middle}
        style={{
          left: ((offsetPage - 2300) / pageHeight) * 100,
        }}
      >
        {activeBooks.slice(5, 11).map((book) => (
          <div
            key={book.id}
            className={cn(s.snippet, {
              [s.fadedBook]: !book.isActive,
              [s.restoredColor]: isRestoredColor,
            })}
          >
            <book.bookImg />
          </div>
        ))}
      </div>
      <div
        className={s.bot}
        style={{
          left: -((offsetPage - 300) / pageHeight) * 100,
        }}
      >
        {activeBooks.slice(12, 17).map((book) => (
          <div
            key={book.id}
            className={cn(s.snippet, {
              [s.fadedBook]: !book.isActive,
              [s.restoredColor]: isRestoredColor,
            })}
          >
            <book.bookImg />
          </div>
        ))}
      </div>
    </div>
  );
}

export default BookSnippets;
