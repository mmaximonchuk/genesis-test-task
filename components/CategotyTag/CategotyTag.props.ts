import { DetailedHTMLProps, HTMLAttributes } from 'react';

export enum CategoriesText {
    All_Categories = "All categories",
    Self_Growth = "Self-Growth",
    Happiness = "Happiness",
    Money_And_Investment = "Money & Investment",
    Negotiation = "Negotiation",
    Health = "Health",
    Spirituality = "Spirituality",
    Business_And_Career = "Business & Career",
    Love_And_Sex = "Love & Sex",
    Productivity = "Productivity",
    Sports_And_Fitness = "Sports & Fitness"
  } 
export interface CategotyTagProps extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement>{
	categoryText: string;
    CategoryImg: React.FunctionComponent<React.SVGAttributes<SVGAElement>> | null;
    isActive: boolean;
    setActive: (categoryId) => void;
}