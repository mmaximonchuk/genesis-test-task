import React from "react";
import cn from "classnames";

import { Typography } from "../Typography/Typography";
import { CategotyTagProps } from "./CategotyTag.props";

import s from "./CategotyTag.module.scss";

function CategotyTag({
  categoryText,
  CategoryImg,
  isActive,
  setActive,
}: CategotyTagProps) {
  return (
    <button
      type="button"
      className={cn(s.categoryTag, {
        [s.active]: isActive,
      })}
      onClick={setActive}
    >
      {CategoryImg !== null && <CategoryImg />}
      <Typography tag="span" className={s.tagTitle}>
        {categoryText}
      </Typography>
    </button>
  );
}

export function Categories({
  activeCategories,
  setActiveCategories,
}): JSX.Element {
  const handleActiveCategories = (categoryId) => {
    setActiveCategories(
      activeCategories.map((category: CategotyTagProps) => {
        if (category.id === categoryId) {
          return {
            ...category, isActive: !category.isActive,
          };
        }
        return category;
      }),
    );
  };

  return (
    <div className={s.categories}>
      <div className={s.top}>
        {activeCategories.slice(0, 6).map((category) => (
          <CategotyTag
            key={category.id}
            CategoryImg={category.CategoryImg}
            categoryText={category.categoryText}
            isActive={category.isActive}
            setActive={() => handleActiveCategories(category.id)}
          />
        ))}
      </div>
      <div className={s.bot}>
        {activeCategories.slice(6, activeCategories.length).map((category) => (
          <CategotyTag
            key={category.id}
            CategoryImg={category.CategoryImg}
            categoryText={category.categoryText}
            isActive={category.isActive}
            setActive={() => handleActiveCategories(category.id)}
          />
        ))}
      </div>
    </div>
  );
}
