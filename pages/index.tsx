import React, { useEffect, useState } from "react";

import NavLink from "next/link";
import Image from "next/image";
import Head from "next/head";
import cn from "classnames";

import { Title, Typography, TestimonialCard, Categories } from "../components";
import { withLayout } from "../layout/Layout";
import BookSnippets from "../components/BookSnippet/BookSnippet";
import { CategoriesText } from "../components/CategotyTag/CategotyTag.props";
import {
  LISTENING_MODE,
  READING_MODE,
  SOURCE_INSTAGRAM,
} from "../helpers/constants";

import IconChart from "../assets/images/homePage/chart.svg";
// import imgChart from "../assets/images/homePage/chart.png";
import imgFsPic from "../assets/images/homePage/fsPic.png";
import IconSsFirst from "../assets/images/homePage/ssFirst.svg";
import IconSsSecond from "../assets/images/homePage/ssSecond.svg";
import IconSsThird from "../assets/images/homePage/ssThird.svg";

import IconThidSecBook from "../assets/images/homePage/book.svg";
import IconThidSecLine from "../assets/images/homePage/bookLine.svg";
import IconThidSecPhone from "../assets/images/homePage/phone.svg";
import IconAudio from "../assets/images/homePage/audio.svg";
import IconText from "../assets/images/homePage/text.svg";
import imgFifthSectionPhone from "../assets/images/homePage/fifthSecBigPhone.png";
import imgFifthSectionRead from "../assets/images/homePage/fifthSectionRead.png";
import imgUserProfile1 from "../assets/images/homePage/photo.png";
import imgUserProfile2 from "../assets/images/homePage/photo2.png";
import imgUserProfile3 from "../assets/images/homePage/photo3.png";
import imgUserProfile4 from "../assets/images/homePage/photo4.png";
import IconAppStore from "../assets/images/homePage/App Store.svg";
import IconPlayMarket from "../assets/images/homePage/Google Play.svg";
import IconStar from "../assets/images/homePage/star-fill.svg";
import IconStarPart1 from "../assets/images/homePage/starPart.svg";
import IconStarPart2 from "../assets/images/homePage/starPart2.svg";
import IconSelfGrowth from "../assets/images/tags/selfGrowth.svg";
import IconHappiness from "../assets/images/tags/happiness.svg";
import IconMoneyAndInvestment from "../assets/images/tags/moneyAndInvestment.svg";
import IconNegotiation from "../assets/images/tags/negotiation.svg";
import IconHealth from "../assets/images/tags/health.svg";
import IconSpiritualActivity from "../assets/images/tags/spiritulity.svg";
import IconProductivity from "../assets/images/tags/productivity.svg";
import IconBusinessAndCareer from "../assets/images/tags/businessAndCareer.svg";
import IconLoveAndSex from "../assets/images/tags/loveAndSex.svg";
import IconSportAndFitness from "../assets/images/tags/sportAndFitness.svg";
import IconBookSnippet1 from "../assets/images/snippets/book-snippet1.svg";
import IconBookSnippet2 from "../assets/images/snippets/book-snippet2.svg";
import IconBookSnippet3 from "../assets/images/snippets/book-snippet3.svg";
import IconBookSnippet4 from "../assets/images/snippets/book-snippet4.svg";
import IconBookSnippet5 from "../assets/images/snippets/book-snippet5.svg";
import IconBookSnippet6 from "../assets/images/snippets/book-snippet6.svg";
import IconBookSnippet7 from "../assets/images/snippets/book-snippet7.svg";
import IconBookSnippet8 from "../assets/images/snippets/book-snippet8.svg";
import IconBookSnippet9 from "../assets/images/snippets/book-snippet9.svg";
import IconBookSnippet10 from "../assets/images/snippets/book-snippet10.svg";
import IconBookSnippet11 from "../assets/images/snippets/book-snippet11.svg";
import IconBookSnippet12 from "../assets/images/snippets/book-snippet12.svg";
import IconBookSnippet13 from "../assets/images/snippets/book-snippet13.svg";
import IconBookSnippet14 from "../assets/images/snippets/book-snippet14.svg";
import IconBookSnippet15 from "../assets/images/snippets/book-snippet15.svg";
import IconBookSnippet16 from "../assets/images/snippets/book-snippet16.svg";

import s from "../styles/Home.module.scss";

const testimonials = [
  {
    id: 1,
    content: `This app simplifies books into super condensed but easy-to-digest
    snippets. Listened to almost all of the Art of Saying No during my
    warm upstretch sesh today. A powerful tool I recommend to anyone
    who’s busy and can’t find time to sit down to read!`,
    rating: 5,
    userImg: imgUserProfile1,
    origin: SOURCE_INSTAGRAM,
  },
  {
    id: 2,
    content:
      "Headway app is one of the best investments I’ve ever made into myself outside of spiritual stuff. Summarized books that you can read or listen to!",
    rating: 5,
    userImg: imgUserProfile2,
    origin: SOURCE_INSTAGRAM,
  },
  {
    id: 3,
    content:
      "In case you often find a book you just started — the Headway app is for you. It’s an audiobook reader that summarized all the key lessons in that great book you’ve been aspiring to read, but you didn’t finish.",
    rating: 5,
    userImg: imgUserProfile3,
    origin: SOURCE_INSTAGRAM,
  },
  {
    id: 4,
    content:
      "So excited for my latest app subscription for self-growth! You can set reading goals, take challenges or read offline! 🤓",
    rating: 5,
    userImg: imgUserProfile4,
    origin: SOURCE_INSTAGRAM,
  },
];

const booksWithCategories = [
  {
    id: 1,
    bookImg: IconBookSnippet1,
    categories: [CategoriesText.All_Categories, CategoriesText.Self_Growth],
    isActive: false,
  },
  {
    id: 2,
    bookImg: IconBookSnippet2,
    categories: [
      CategoriesText.All_Categories,
      CategoriesText.Self_Growth,
      CategoriesText.Productivity,
    ],
    isActive: false,
  },
  {
    id: 3,
    bookImg: IconBookSnippet3,
    categories: [CategoriesText.All_Categories, CategoriesText.Love_And_Sex],
    isActive: false,
  },
  {
    id: 4,
    bookImg: IconBookSnippet4,
    categories: [
      CategoriesText.All_Categories,
      CategoriesText.Self_Growth,
      CategoriesText.Spirituality,
    ],
    isActive: false,
  },
  {
    id: 5,
    bookImg: IconBookSnippet5,
    categories: [CategoriesText.All_Categories, CategoriesText.Negotiation],
    isActive: false,
  },
  {
    id: 6,
    bookImg: IconBookSnippet6,
    categories: [
      CategoriesText.All_Categories,
      CategoriesText.Self_Growth,
      CategoriesText.Productivity,
    ],
    isActive: false,
  },
  {
    id: 7,
    bookImg: IconBookSnippet7,
    categories: [
      CategoriesText.All_Categories,
      CategoriesText.Self_Growth,
      CategoriesText.Health,
      CategoriesText.Productivity,
    ],
    isActive: false,
  },
  {
    id: 8,
    bookImg: IconBookSnippet8,
    categories: [
      CategoriesText.All_Categories,
      CategoriesText.Self_Growth,
      CategoriesText.Happiness,
      CategoriesText.Love_And_Sex,
    ],
    isActive: false,
  },
  {
    id: 9,
    bookImg: IconBookSnippet9,
    categories: [
      CategoriesText.All_Categories,
      CategoriesText.Self_Growth,
      CategoriesText.Happiness,
      CategoriesText.Productivity,
    ],
    isActive: false,
  },
  {
    id: 10,
    bookImg: IconBookSnippet10,
    categories: [
      CategoriesText.All_Categories,
      CategoriesText.Self_Growth,
      CategoriesText.Health,
      CategoriesText.Spirituality,
    ],
    isActive: false,
  },
  {
    id: 11,
    bookImg: IconBookSnippet11,
    categories: [
      CategoriesText.All_Categories,
      CategoriesText.Self_Growth,
      CategoriesText.Money_And_Investment,
      CategoriesText.Business_And_Career,
      CategoriesText.Love_And_Sex,
    ],
    isActive: false,
  },
  {
    id: 12,
    bookImg: IconBookSnippet12,
    categories: [
      CategoriesText.All_Categories,
      CategoriesText.Negotiation,
      CategoriesText.Spirituality,
    ],
    isActive: false,
  },
  {
    id: 13,
    bookImg: IconBookSnippet13,
    categories: [CategoriesText.All_Categories, CategoriesText.Negotiation],
    isActive: false,
  },
  {
    id: 14,
    bookImg: IconBookSnippet14,
    categories: [
      CategoriesText.All_Categories,
      CategoriesText.Money_And_Investment,
      CategoriesText.Business_And_Career,
    ],
    isActive: false,
  },
  {
    id: 15,
    bookImg: IconBookSnippet15,
    categories: [
      CategoriesText.All_Categories,
      CategoriesText.Money_And_Investment,
      CategoriesText.Business_And_Career,
    ],
    isActive: false,
  },
  {
    id: 16,
    bookImg: IconBookSnippet16,
    categories: [
      CategoriesText.All_Categories,
      CategoriesText.Health,
      CategoriesText.Sports_And_Fitness,
    ],
    isActive: false,
  },
];

const categories = [
  {
    id: 1,
    categoryText: CategoriesText.All_Categories,
    CategoryImg: null,
    isActive: false,
  },
  {
    id: 2,
    categoryText: CategoriesText.Self_Growth,
    CategoryImg: IconSelfGrowth,
    isActive: false,
  },
  {
    id: 3,
    categoryText: CategoriesText.Happiness,
    CategoryImg: IconHappiness,
    isActive: false,
  },
  {
    id: 4,
    categoryText: CategoriesText.Money_And_Investment,
    CategoryImg: IconMoneyAndInvestment,
    isActive: false,
  },
  {
    id: 5,
    categoryText: CategoriesText.Negotiation,
    CategoryImg: IconNegotiation,
    isActive: false,
  },
  {
    id: 6,
    categoryText: CategoriesText.Health,
    CategoryImg: IconHealth,
    isActive: false,
  },
  {
    id: 7,
    categoryText: CategoriesText.Spirituality,
    CategoryImg: IconSpiritualActivity,
    isActive: false,
  },
  {
    id: 8,
    categoryText: CategoriesText.Business_And_Career,
    CategoryImg: IconBusinessAndCareer,
    isActive: false,
  },
  {
    id: 9,
    categoryText: CategoriesText.Love_And_Sex,
    CategoryImg: IconLoveAndSex,
    isActive: false,
  },
  {
    id: 10,
    categoryText: CategoriesText.Productivity,
    CategoryImg: IconProductivity,
    isActive: false,
  },
  {
    id: 11,
    categoryText: CategoriesText.Sports_And_Fitness,
    CategoryImg: IconSportAndFitness,
    isActive: false,
  },
];
function Home(): JSX.Element {
  const [mode, setMode] = useState<string>(LISTENING_MODE);
  const [firstScreen, setFirstScreen] = useState<boolean>(true);
  const [offsetPage, setOffsetoffsetPage] = useState<number>(0);
  const [activeBooks, setActiveBooks] = useState(booksWithCategories);
  const [activeCategories, setActiveCategories] = useState(categories);

  const handleScroll = () => {
    setOffsetoffsetPage(window.pageYOffset);
  };

  useEffect(() => {
    window.addEventListener("scroll", handleScroll);
    return () => window.removeEventListener("scroll", handleScroll);
  }, []);

  useEffect(() => {
    if (offsetPage > 490) {
      setFirstScreen(false);
    } else {
      setFirstScreen(true);
    }
  }, [offsetPage]);

  return (
    <div className={s.container}>
      <Head>
        <title>Test Task</title>
        <meta name="description" content="Ohh, my! Here we go again!" />
        <meta property="og:title" content="(Genesis) Test Task" />
        <meta property="og:description" content="Just click and relax! " />
        <meta property="og:type" content="article" />
      </Head>
      <section className={cn(s.section, s.firstSection)}>
        <div className={s.fsLeft}>
          <Title tag="h1" className={s.title}>
            Become a better you
          </Title>
          <Typography tag="p" className={s.fsDescription}>
            with 15-min bite-sized reads and audios of the world’s best
            nonfiction books
          </Typography>

          <IconChart />
          {/* <Image src={imgChart} alt="Rates" quality={100}/> */}

          <NavLink href="#">
            <a
              className={cn(s.ctaButton, {
                [s.fixed]: firstScreen === false,
              })}
            >
              <Typography tag="p">Get Started</Typography>
            </a>
          </NavLink>
        </div>
        <div className={s.fsRight}>
          <Image
            layout="intrinsic"
            src={imgFsPic}
            alt="discover"
            className={s.fsRightPicture}
          />
        </div>
      </section>
      <section className={cn(s.section, s.secondSection)}>
        <Title tag="h1" className={s.title}>
          Get smarter with the least effort
        </Title>
        <div className={s.benefits}>
          <div className={s.benefit}>
            <IconSsFirst />
            <div className={s.benefitTexts}>
              <Title tag="h2">Learn from the best</Title>
              <Typography tag="p">
                Highlighted by top reviewers, we picked only the most
                outstanding bestsellers for you.
              </Typography>
            </div>
          </div>
          <div className={s.benefit}>
            <IconSsSecond />

            <div className={s.benefitTexts}>
              <Title tag="h2">Get key insights</Title>
              <Typography tag="p">
                Major ideas in a bite-sized form. Long reads no more!
              </Typography>
            </div>
          </div>
          <div className={s.benefit}>
            <IconSsThird />
            <div className={s.benefitTexts}>
              <Title tag="h2">Build a reading habit</Title>
              <Typography tag="p">
                Consistency is the key to top performance. Only 15 minutes per
                day to make reading your brand new habit!
              </Typography>
            </div>
          </div>
        </div>
      </section>
      <section className={cn(s.section, s.thirdSection)}>
        <Title tag="h1" className={s.title}>
          Summary is the new black
        </Title>
        <Typography tag="p" className={s.thirdSectionDescription}>
          Use summaries: comprehensive 15-min abstracts from nonfiction books
          with the main concepts and recommendations.
        </Typography>
        <div className={s.exampleContainer}>
          <div className={s.bookBlock}>
            <IconThidSecBook />
            <Typography tag="span">Typical book</Typography>
            <Typography tag="span" className={s.fadedText}>
              320 pages ~ 20 hours
            </Typography>
          </div>
          <IconThidSecLine className={s.devider} />
          <Typography tag="span" className={s.centerBlock}>
            Key ideas & insights
          </Typography>
          {/* <IconThidSecArrow className={cn(s.devider, s.arrow)} /> */}
          <div className={s.arrow}>
            <IconThidSecLine className={s.devider} />
          </div>
          <div className={s.phoneBlock}>
            <IconThidSecPhone />
            <Typography tag="span">Summary</Typography>
            <Typography tag="span" className={s.fadedText}>
              ~ 15 minutes
            </Typography>
          </div>
        </div>
      </section>
      <section className={cn(s.section, s.fourthSection)}>
        <Title tag="h1" className={s.fourthSectionTitle}>
          Determine your goal and go ahead!
        </Title>
        <Typography tag="p" className={s.fourthSectionDescription}>
          Learn something new every day from Self-Growth, Happiness, Money,
          Health, and many other categories. Benefit from unlimited access to
          world best thoughts.
        </Typography>

        <Categories
          activeCategories={activeCategories}
          setActiveCategories={setActiveCategories}
        />
        <BookSnippets
          activeBooks={activeBooks}
          activeCategories={activeCategories}
          setActiveBooks={setActiveBooks}
        />
      </section>
      <section className={cn(s.section, s.fifthSection)}>
        <div className={s.fifthSectionLeft}>
          <Title tag="h1" className={s.title}>
            Read or Listen
          </Title>
          <Typography tag="p" className={s.fifthSectionDescription}>
            Commuting, jogging, or stuck in line? Enjoy the best books in a
            condensed format. Summaries have an audio version, narrated by
            professional voice actors.
          </Typography>

          {/* <IconChart /> */}
          <div className={s.togglerContainer}>
            <Typography
              tag="span"
              className={cn({
                [s.fadedText]: mode === READING_MODE,
              })}
            >
              Listen
            </Typography>
            <div className={s.togglerButtons}>
              <button
                type="button"
                onClick={() => setMode(LISTENING_MODE)}
                className={cn(s.modeBtn, s.btnAudio, {
                  [s.active]: mode === LISTENING_MODE,
                })}
              >
                <IconAudio />
              </button>
              <button
                type="button"
                onClick={() => setMode(READING_MODE)}
                className={cn(s.modeBtn, s.btnText, {
                  [s.active]: mode === READING_MODE,
                })}
              >
                <IconText />
              </button>
            </div>
            <Typography
              tag="span"
              className={cn({
                [s.fadedText]: mode === LISTENING_MODE,
              })}
            >
              Read
            </Typography>
          </div>
        </div>
        <div className={s.fifthSectionRight}>
          <Image
            layout="intrinsic"
            src={
              mode === READING_MODE ? imgFifthSectionRead : imgFifthSectionPhone
            }
            alt={`${
              mode === READING_MODE ? "Text for reading" : "Text for listening"
            }`}
            className={s.fifthSectionRightPicture}
            quality={100}
          />
        </div>
      </section>
      <section className={cn(s.section, s.sixthSection)}>
        <Title tag="h1">People love the Headway app</Title>
        <Typography tag="p" className={s.sixthSectionDescription}>
          Become a member of our global community of
          {" "}
          <Typography tag="span" className={s.bluedText}>
            7 million people
          </Typography>
        </Typography>
        <div className={s.testimonials}>
          {testimonials.map(({ id, content, userImg, rating, origin }) => (
            <TestimonialCard
              key={id}
              content={content}
              userImg={userImg}
              rating={rating}
              origin={origin}
            />
          ))}
        </div>
        <div className={s.sources}>
          <div className={s.sourceCard}>
            <div className={s.ratingNumber}>
              <Title tag="h2">4.7</Title>
              <IconAppStore />
            </div>
            <div className={s.ratingStars}>
              <IconStar />
              <IconStar />
              <IconStar />
              <IconStar />
              <IconStarPart1 />
            </div>
            <Typography className={s.totalVoted} tag="span">
              50K ratings
            </Typography>
          </div>
          <div className={s.sourceCard}>
            <div className={s.ratingNumber}>
              <Title className={s.green} tag="h2">
                4.2
              </Title>
              <IconPlayMarket />
            </div>
            <div className={s.ratingStars}>
              <IconStar />
              <IconStar />
              <IconStar />
              <IconStar />
              <IconStarPart2 />
            </div>
            <Typography className={s.totalVoted} tag="span">
              31k ratings
            </Typography>
          </div>
        </div>
      </section>
    </div>
  );
}

// export interface HomeProps extends Record<string, unknown> {}

export default withLayout(Home);
