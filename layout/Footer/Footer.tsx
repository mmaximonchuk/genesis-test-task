import React from "react";
import NavLink from "next/link";
import Image from "next/image";
import cn from "classnames";
import { Title, Typography } from "../../components";

import iconLogo from "../../assets/header/Logo.png";
import IconFb from "../../assets/footer/Facebook.svg";
import IconInst from "../../assets/footer/Instagram.svg";
import IconTwitter from "../../assets/footer/Twitter.svg";

import s from "./Footer.module.scss";

export default function Footer(): JSX.Element {
  return (
    <footer className={s.footer}>
      <div className={cn("c-white-container", s.footerInner)}>
        <NavLink href="/">
          <a className={s.footerLogo}>
            <Image
              src={iconLogo}
              alt="Headway"
              layout="fixed"
              width={122}
              height={25}
            />
          </a>
        </NavLink>
        <Title tag="h1" className={s.footerReminder}>
          Empower yourself with the best books insights
        </Title>

        <div className={s.footerSocialsWrapper}>
          <Typography tag="span">Social media:</Typography>
          <div className={s.footerLinks}>
            <NavLink href="/">
              <a className={s.footerLink}>
                <IconFb />
              </a>
            </NavLink>
            <NavLink href="/">
              <a className={s.footerLink}>
                <IconInst />
              </a>
            </NavLink>
            <NavLink href="/">
              <a className={s.footerLink}>
                <IconTwitter />
              </a>
            </NavLink>
          </div>
        </div>
      </div>
    </footer>
  );
}
