import React from "react";
import NavLink from "next/link";
import Image from "next/image";

import iconLogo from "../../assets/header/Logo.png";
import IconUser from "../../assets/header/userpic.svg";
import IconMore from "../../assets/header/Union.svg";

import s from "./Header.module.scss";

export default function Header(): JSX.Element {
  return (
    <header className={s.header}>
      <div className={s.headerInner}>
        <NavLink href="/">
          <a className={s.logo}>
            <Image layout="fixed" alt="Headway" width={122} height={25} src={iconLogo} />
          </a>
        </NavLink>
        <nav className={s.nav}>
          <button type="button" className={s.navBtn}>
            <IconUser />
          </button>
          <button type="button" className={s.navBtn}>
            <IconMore />
          </button>
        </nav>
      </div>
    </header>
  );
}
